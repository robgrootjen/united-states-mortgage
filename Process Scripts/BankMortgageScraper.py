import requests 
from bs4 import BeautifulSoup
import csv

#Request web content
result = requests.get('https://www.ratehub.ca/banks/bank-mortgage-rates')

#Save content in variable.
src = result.content

#Soup
soup = BeautifulSoup(src, 'lxml')

#Look for table and save in CSV
table = soup.find('table')
with open('bankrate.csv','w', newline = '') as f:
    writer = csv.writer(f)
    for tr in table('tr'):
        row = [t.get_text(strip=True) for t in tr(['td','th'])]
        cell1 = row[0]
        cell2 = row[1][:5]
        row = [cell1,cell2]
        writer.writerow(row)