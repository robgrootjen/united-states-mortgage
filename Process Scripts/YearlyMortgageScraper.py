import requests
from bs4 import BeautifulSoup
import csv

#Requests webpage content
result = requests.get('https://www.valuepenguin.com/mortgages/historical-mortgage-rates#nogo')

#Save content in variable
src = result.content

#SoupActivate
soup = BeautifulSoup(src,'lxml')

#Look for table and save the CSV
table = soup.find('table')
with open('mortgage.csv', 'w', newline='') as f:
    writer = csv.writer(f)
    for tr in table('tr')[:-1]:
        row = [t.get_text(strip=True) for t in tr(['td','th'])]
        cell1 = row[0]
        cell2 = row[1].replace('%','')
        cell3 = row[2].replace(',','').replace('%','')
        cell4 = row[3].replace('%','')
        row = [cell1,cell2,cell3,cell4]
        writer.writerow(row)