The data sets included shows the yearly highest, lowest and average mortgage rate since 1971 and also the current 
mortgage rate for every and each bank.

## Data
The data is sourced from: 
* Yearly historical mortgage rates - https://www.valuepenguin.com/mortgages/historical-mortgage-rates#nogo
* Current bank mortgage rates - https://www.ratehub.ca/banks/bank-mortgage-rates

The repo includes:
* datapackage.json
* bankrate.csv
* mortgage.csv
* 2 scrapers.
    * YearlyMortgageScraper.py
    * BankMortgageScraper.py

## Preparation

***Requires:***
1. Python
2. Visualstudio / Jupyter notebook / or any platform that works with python.

***bankrate.csv*** (Shows current mortgage rate of every bank.)
* The CSV Data has 2 colummns. 
    * Provider
    * 5-YR

***mortgage.csv*** (Shows historical yearly mortgage rates.)
* The CSV Data has 4 colummns.
    * Year
    * Lowest Rate
    * Highest Rate
    * Average Rate

***datapackage.json***
* The json file has all the information from the two csv files, licence, author and 2 line graphs for every table.

***BankMortgageScraper.py***
* This script will scrape the current mortgage rate of every bank from this source - 
    * https://www.ratehub.ca/banks/bank-mortgage-rates

***YearlyMortgageScraper.py***
* This script will scrape the historical mortgage rates table from this source - 
    * https://www.valuepenguin.com/mortgages/historical-mortgage-rates#nogo 

***Instructions:***
* Copy any of the two script in the "Process Scripts" folder
* Open in jupyter notebook, python shell, VS code, or any preferred platform.
* Run the code
* Csv file will be saved in document where your terminal is at the moment.

## Licence
This Data Package is made available under the Public Domain Dedication and License v1.0 